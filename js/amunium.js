"use strict";

$(document).ready(function() {
    
    $('form.ajax-form').on('submit', function(event) {
        event.preventDefault();
        var form = $(this);
        clearInvalidInput(form);
        
        var inputs = form.find('#name, #email, #message');
        var halt = false;
        for (var i in inputs) {
            if (inputs[i].value == '') {
                invalidateInput(form, inputs[i].name);
                halt = true;
            }
        }
        if (halt)
            return;
        
        $.ajax({
            url: form.attr('action'),
            method: 'post',
            data: form.serializeArray()
        })
        .done(function(data) {
            if (data.status == 1) {
                form.hide();
                $('#contact-confirmation').show();
            }
            else {
                for (var i in data.invalid_fields) {
                    invalidateInput(form, data.invalid_fields[i]);
                }
            }
        });
    });
    
    
    $('a.img-viewer, img-viewer a').click(function(event) {
        event.preventDefault();
        
        if ($('#img-viewer-container').length == 0)
            $('body').append('<div id="img-viewer-container"></div>');
        var container = $('#img-viewer-container');
        container.toggleClass('squished', false);
        container.click(function() {
            container.hide();
        });
        
        var image = new Image();
        image.onload = function() {
            var wwidth = $(window).width();
            var wheight = $(window).height();
            var imgwidth = Math.min(wwidth, image.width);
            var imgheight = Math.floor(image.height / (image.width / imgwidth));
            
            var pad = 20;
            if (imgwidth >= wwidth - 40) {
                pad = 0;
                container.toggleClass('squished', true);
            }
            
            container.html('<img class="fill" src="'+ image.src +'">');
            container.width(imgwidth);
            container.height(imgheight);
            container.css('left', Math.floor(wwidth / 2) - (imgwidth / 2 + pad) + 'px');
            container.css('top', Math.floor((wheight / 2) - (image.height / 2 + pad) + $(window).scrollTop() ) + 'px');
            
            container.fadeIn();
        }
        image.src = $(this).attr('href');
    });
    
    $('body').click(function() {
        $('#img-viewer-container').hide();
    });
});

function clearInvalidInput(form) {
    form.find('input').removeClass('invalid');
}

function invalidateInput(form, name) {
    var el = form.find('#' + name);
    el.addClass('invalid');
}