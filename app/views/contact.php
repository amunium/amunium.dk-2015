@extends layouts/master

<h1>Contact</h1>

<div class="block distance">
    <h4>Location</h4>
    
    <p>
        Rødovre, Copenhagen, Denmark.
    </p>
    
    <form role="form" class="ajax-form" method="post" action="{{ App::root() }}/send_contact_mail">
        <h4 class="distance">Get in touch</h4>
        
        <div class="form-group">
            <label for="name">Name</label>
            <input id="name" name="name" type="text" placeholder="Name">
        </div>
        
        <div class="form-group">
            <label for="email">E-mail</label>
            <input id="email" name="email" type="text" placeholder="E-mail">
        </div>
        
        <div class="form-group">
            <label for="message">Message</label>
            <textarea id="message" name="message" rows="10" cols="20"></textarea>
        </div>
        
        <div class="form-group distance">
            <input type="hidden" name="token" value="{{ token() }}">
            <input type="text" name="address" class="hp" value="">
            <input type="submit" class="btn btn-md btn-std" value="Send message">
        </div>
    </form>
    
    <div id="contact-confirmation" class="latent">
        <h2>Thanks</h2>
        
        <p>
            I will get back to you.
        </p>
    </div>
</div>