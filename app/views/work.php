@extends layouts/master

<h1>Work</h1>

<div class="distance block">
    <p>
        I am primarily focused on back-end development in PHP.<br>
    </p>
    <p>
        This website is built with my <a href="https://bitbucket.org/amunium/anano-2/src" class="btn-link" target="_blank">Anano PHP Framework</a>.
        You can view the full source code for this site <a href="https://bitbucket.org/amunium/amunium.dk-2015/src" class="btn-link" target="_blank">here</a>.
    </p>
</div>

<h2 class="distance">Examples</h2>

<div class="row tiles">
    <div class="col-sm-4">
        <a class="img-viewer" href="/img/tiles/sb-dtp-lg.png">
            <img src="/img/tiles/sb-dtp.jpg">
        </a>
        <div class="caption">
            <span class="work-types">
                <abbr class="be" title="Back-end">BE</abbr>
            </span>
            <a class="img-viewer" href="/img/tiles/sb-dtp-lg.png">
                Online sales catalogs
                <cite>Superbrugsen, Kvickly, Fakta</cite>
            </a>
        </div>
    </div>
    
    <div class="col-sm-4">
        <a class="img-viewer" href="/img/tiles/rp-kiosk-lg.png">
            <img src="/img/tiles/rp-kiosk.jpg">
        </a>
        <div class="caption">
            <span class="work-types">
                <abbr class="be" title="Back-end">BE</abbr><abbr class="fe" title="Front-end">FE</abbr>
            </span>
            <a class="img-viewer" href="/img/tiles/rp-kiosk-lg.png">
                Kiosk screen admin system
                <cite>Republica A/S</cite>
            </a>
        </div>
    </div>
    
    <div class="col-sm-4">
        <a class="img-viewer" href="/img/tiles/ue-admin-lg.png">
            <img src="/img/tiles/ue-admin.jpg">
        </a>
        <div class="caption">
            <span class="work-types">
                <abbr class="be" title="Back-end">BE</abbr><abbr class="fe" title="Front-end">FE</abbr>
            </span>
            <a class="img-viewer" href="/img/tiles/ue-admin-lg.png">
                Custom CMS for news website
                <cite>Odsgard A/S, UgensErhverv.dk</cite>
            </a>
        </div>
    </div>
</div>

<h2>References</h2>

<p>
    These are some sites I have worked on, either alone or as part of a team.<br>
    Several are no longer available, require login or have been redone since.
</p>

<div class="row">
    @for ($i = 0; $i < 3; $i++):
        <ul class="col-md-4">
            @foreach ($list[$i] as $item):
                <li><a rel="nofollow" target="_blank" href="http://{{ $item }}">{{ $item }}</a></li>
            @endforeach
        </ul>
    @endfor
</div>
