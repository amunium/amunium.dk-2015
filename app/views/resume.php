@extends layouts/master

<h1>Résumé</h1>

<div class="row">
    <div class="col-md-6">
        
        <h3>Work</h3>
        
        <div class="block">
            
            <div class="section">
                <strong>Back-end web developer</strong><br>
                <small>December 2013 - now</small><br>
                <a href="http://republica.dk" target="_blank">Republica A/S</a>
                
                <p class="description">
                    Republica is an advertising agency owned by <a href="http://coop.dk">Coop</a>.<br>
                    As the primary back-end developer, I am responsible for their digital sales catalogs,
                    as well as a constant flow of high-load campaign websites.
                </p>
            </div>
            
            <div class="section">
                <strong>Full stack web developer</strong><br>
                <small>2008 - December 2013</small><br>
                <a href="http://odsgard.dk" target="_blank">Odsgard A/S</a>
                
                <p class="description">
                    Odsgard is a family owned markering bureau and publishing house.<br>
                    I was responsible for both front and back-end development on their many news sites,
                    as well as development and maintenance of several independent customer websites,
                    server maintenance and general internal tech support.
                </p>
            </div>
            
            <div class="section">
                <strong>Full stack web developer, project based</strong><br>
                <small>2007</small><br>
                <a href="http://ku.dk" target="_blank">University of Copenhagen</a>
                
                <p class="description">
                    Several time limited websites for the Faculty of Theology.
                </p>
            </div>
            
        </div>
        
    </div>
    
    <div class="col-md-6">
        
        <h3>Education</h3>
        
        <div class="block">
            
            <p>
                <strong>Multimedia Designer (MMD-AK)<br></strong>
                <small>2005-2007</small><br>
                <a href="http://kea.dk/" target="_blank">Copenhagen School of Design and Technology / Københavns Erhvervsakademi</a>
            </p>
            
        </div>
        
    </div>
</div>