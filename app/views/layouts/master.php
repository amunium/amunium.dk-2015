<!DOCTYPE html>
<html>
<head>
<title>{{ $title | "" }}Amunium.dk</title>
<meta charset="UTF-8">
<meta name="viewport" content="initial-scale=1.0" />
<link rel="shortcut icon" href="/img/icons/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon" href="/img/icons/apple-touch-icon.png" />
<link rel="apple-touch-icon" sizes="57x57" href="/img/icons/apple-touch-icon-57x57.png" />
<link rel="apple-touch-icon" sizes="72x72" href="/img/icons/apple-touch-icon-72x72.png" />
<link rel="apple-touch-icon" sizes="76x76" href="/img/icons/apple-touch-icon-76x76.png" />
<link rel="apple-touch-icon" sizes="114x114" href="/img/icons/apple-touch-icon-114x114.png" />
<link rel="apple-touch-icon" sizes="120x120" href="/img/icons/apple-touch-icon-120x120.png" />
<link rel="apple-touch-icon" sizes="144x144" href="/img/icons/apple-touch-icon-144x144.png" />
<link rel="apple-touch-icon" sizes="152x152" href="/img/icons/apple-touch-icon-152x152.png" />
<link type="text/css" rel="stylesheet" href="/packages/bootstrap/css/bootstrap.min.css">
<link type="text/css" rel="stylesheet" href="/css/amunium.css">
<script src="/packages/jquery/js/jquery-1.11.1.min.js"></script>
<script src="/packages/bootstrap/js/bootstrap.min.js"></script>
<script src="/js/amunium.js"></script>
@partial partials/analytics
</head>
<body>

<nav class="navbar navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed"
                data-toggle="collapse" data-target="#navbar" aria-expanded="false"
                aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ App::root() ?: '/' }}">Amunium.dk</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li{{ App::current() == '/' ? ' class="active"' : '' }}><a href="{{ App::root() ?: '/' }}">Home</a></li>
                <li{{ App::current() == '/about' ? ' class="active"' : '' }}><a href="/about">About</a></li>
                <li{{ App::current() == '/work' ? ' class="active"' : '' }}><a href="/work">Work</a></li>
                <li{{ App::current() == '/resume' ? ' class="active"' : '' }}><a href="/resume">Résumé</a></li>
                <li{{ App::current() == '/contact' ? ' class="active"' : '' }}><a href="/contact">Contact</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="container">
    <div id="content">
        @content
    </div>
</div>

</body>
</html>