@extends layouts/master

<h1>About</h1>

<div class="row">
    <div class="col-md-6">
        
        <h3>Kris Lux</h3>
        
        <p>
            <img class="fill" src="/img/kl.jpg" alt="Kris Lux">
        </p>
        
        <ul class="display distance-right">
            <li>
                <a href="https://bitbucket.org/amunium/" target="_blank" class="btn btn-std btn-with-icon">
                    <img alt="Bitbucket" src="/img/icon_bb.png" style="vertical-align: -50%;"> Bitbucket
                </a>
            </li>
            
            <li>
                <a href="http://dk.linkedin.com/in/krislux" target="_blank" class="btn btn-std btn-with-icon">
                    <img alt="Bitbucket" src="/img/icon_li.png" style="vertical-align: -50%;"> Linkedin
                </a>
            </li>
        </ul>
        
    </div>
    
    <div class="col-md-6">
        
        <h3>Experience</h3>
        
        <div class="block">
            <h4>PHP</h4>
            
            <p>
                Frameworks: Laravel, CodeIgniter, some Symfony, Fuel, Yii, Slim, Silex, Kohana.
            </p>
            
            <p>
                Other: MySQL, SQLite, ActiveRecord, Codeception, Composer, PhpDoc.
            </p>
            
            <h4 class="distance">.NET</h4>
            
            <p>
                Languages: C#, SQL (SQL Server), some Visual Basic, C++.
            </p>
            
            <p>
                Frameworks: Umbraco (Certified Level 2 Developer), MS MVC.
            </p>
            
            <h4 class="distance">Front-end</h4>
            
            <p>
                HTML(5), CSS(3), LESS, Adobe Photoshop, Responsive design, Bootstrap, DOM Javascript, JQuery, Mootools.
            </p>
            
            <h4 class="distance">Other technologies</h4>
            
            <p>
                Git, SVN, Linux (mainly Lubuntu, Debian), Windows Server, Node.js.
            </p>
        </div>
        
    </div>
</div>