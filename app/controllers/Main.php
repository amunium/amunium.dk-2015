<?php

class Main extends Controller
{
    /**
     * You can use the constructor to add filters to the entire controller class.
     * Filters must evaluate TRUE for the controller to run. You can view and add filters in config/filters.php
     * 
     * To only filter some routes, you can pass the filter function an array as the second parameter containing
     * any or all of the keys 'on', containing http verbs, or 'only' and 'except' containing method names.
     */
    
    function __construct()
    {
        $this->filter('csrf', array('on' => 'POST'));
    }
    
    function index()
    {
        return new View('home');
    }
    
    function about()
    {
        return new View('about', array('title' => 'About | '));
    }
    
    function work()
    {
        // Get list of work and split it into three parts for columns.
        $list = array_map('trim', file(STORAGE_DIR . '/work'));
        $list = array_chunk($list, ceil(count($list) / 3));
        
        return new View('work', array('title' => 'Work | ', 'list' => $list));
    }
    
    function resume()
    {
        return new View('resume', array('title' => 'Résumé | '));
    }
    
    function contact()
    {
        return new View('contact', array('title' => 'Contact | '));
    }
    
    
    function postSendContactMail()
    {
        $rv = array('status' => 0);
        
        $name = Input::get('name');
        $email = Input::get('email');
        $text = Input::get('message');
        
        if (!$name)
            $rv['invalid_fields'][] = 'name';
        // Custom e-mail validation filter because PHP's built in still does not support Unicode domains.
        if (!$email || preg_match('/[^\s]+\@[^\s]+\.[a-z]{2,8}/i', $email) !== 1)
            $rv['invalid_fields'][] = 'email';
        if (!$text)
            $rv['invalid_fields'][] = 'message';
        
        // Honeypot. 'address' is a hidden field, if anything is in it, discard entire post.
        if (empty($rv['invalid_fields']) && !Input::get('address'))
        {
            $message = Swift_Message::newInstance()
            ->setSubject('Message from Amunium.dk')
            ->setFrom(array('no-reply@amunium.dk' => $name . ' (Amunium.dk)'))
            ->setReplyTo(array($email => $name))
            ->setTo(array('email@amunium.dk'))
            ->setBody($text);
            
            $transport = Swift_SmtpTransport::newInstance('localhost', 25);
            $mailer = Swift_Mailer::newInstance($transport);
            $rv['status'] = $mailer->Send($message);
        }
        
        return Response::json($rv);
    }
}